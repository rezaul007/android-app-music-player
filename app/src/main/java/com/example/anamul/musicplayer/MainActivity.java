package com.example.anamul.musicplayer;
import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {
    private ListView lv;
    private String[] list;
    private boolean isEmpty = true;
    private ArrayList<File> mysong;
    private String song;
    private TextView songname;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lv = (ListView)findViewById(R.id.songListView);
        songname = (TextView)findViewById(R.id.textview);

        mysong = songs(Environment.getExternalStorageDirectory());
        list = new String[mysong.size()];
        if(isEmpty == true){
            Toast.makeText(MainActivity.this,"no songs found",Toast.LENGTH_LONG);
        }

        for(int i=0;i<mysong.size();i++){

            list[i]=mysong.get(i).getName().toString();
        }
        lv.setAdapter(new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_list_item_1,list) );
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(song != " "){
                    song=null;
                }
                song = mysong.get(position).getName().toString();
                songname.setText(song);
                Intent intent = new Intent(MainActivity.this,Main2Activity.class);
                intent.putExtra("songs",mysong);
                intent.putExtra("position",position);
                startActivity(intent);
            }
        });
    }
    public ArrayList<File> songs(File root){
        ArrayList<File> al = new ArrayList<File>();
        File[] files = root.listFiles();
        for( File file:files ){
            if(file.isDirectory()){
                al.addAll(songs(file));
                isEmpty = false;
            }
            else if(file.getName().endsWith(".mp3")){

                    al.add(file);
                   isEmpty= false;

            }
        }
        return al;
    }

}
