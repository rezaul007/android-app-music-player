package com.example.anamul.musicplayer;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.io.File;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class Main2Activity extends AppCompatActivity {
    static MediaPlayer mediaPlayer;
    private ArrayList<File> mysong;
    private int i;
    private Button pauseOrStop,left,right,next,previous;
    private TextView songName,duration;
    private String sn;
    private String du;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        if(mediaPlayer != null){
            mediaPlayer.stop();
            mediaPlayer.release();
        }
        Bundle b = getIntent().getExtras();
        mysong = (ArrayList) b.getParcelableArrayList("songs");
        i = b.getInt("position");
        Uri u = Uri.parse(mysong.get(i).toString() );
        mediaPlayer = MediaPlayer.create(Main2Activity.this,u);
        mediaPlayer.start();

        duration = (TextView)findViewById(R.id.duration);
        du=String.valueOf((mediaPlayer.getDuration()/1000)/60);
        duration.setText("Duration :"+du+"min");
        songName = (TextView)findViewById(R.id.songname);
        sn = mysong.get(i).getName().toString();
        songName.setText("Song title :"+sn);
        pauseOrStop = (Button) findViewById(R.id.pauseOrStop);
        left =(Button)findViewById(R.id.left);
        right =(Button)findViewById(R.id.right);
        next = (Button)findViewById(R.id.next);
        previous =(Button)findViewById(R.id.pre);

        pauseOrStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mediaPlayer.isPlaying()){
                    mediaPlayer.pause();
                }
                else{
                    mediaPlayer.start();
                }
            }
        });
       next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.stop();
                mediaPlayer.release();
                i= (i+1) %mysong.size();
                Uri u = Uri.parse(mysong.get(i).toString() );
                mediaPlayer = MediaPlayer.create(Main2Activity.this,u);
                mediaPlayer.start();
                sn = mysong.get(i).getName().toString();
                songName.setText("Song title :"+sn);
                du=String.valueOf((mediaPlayer.getDuration()/1000)/60);
                duration.setText("Duration :"+du+"min");
            }
        });
       previous.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               mediaPlayer.stop();
               mediaPlayer.release();
               if(i-1<0){
                   i=mysong.size()-1;
               }
               else{
                   i=i-1;
               }
               Uri u = Uri.parse(mysong.get(i).toString() );
               mediaPlayer = MediaPlayer.create(Main2Activity.this,u);
               mediaPlayer.start();
               sn = mysong.get(i).getName().toString();
               songName.setText("Song title :"+sn);
               du=String.valueOf((mediaPlayer.getDuration()/1000)/60);
               duration.setText("Duration :"+du+"min");
           }
       });
        left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.seekTo(mediaPlayer.getCurrentPosition()-15000);
            }
        });
        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.seekTo(mediaPlayer.getCurrentPosition()+15000);
            }
        });

    }
}
